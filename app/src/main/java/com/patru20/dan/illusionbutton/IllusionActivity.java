package com.patru20.dan.illusionbutton;

import android.animation.Animator;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewPropertyAnimator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import java.util.Random;

public class IllusionActivity extends AppCompatActivity {

    Button pressButton;
    Switch mySwitch;
    private int height , width;
    boolean isSwitchedOff = true;
    private CompoundButton.OnCheckedChangeListener mListener = null;
    CountDownTimer cTimer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_illusion);

        pressButton = (Button) findViewById(R.id.press_button);
        mySwitch = (Switch) findViewById(R.id.switch1);

        Toast.makeText(getApplicationContext(), "Press button for 1 minute, then the switch turns on",
                Toast.LENGTH_LONG).show();

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    isSwitchedOff = false;
                    mListener = this;
                }
                else{
                    isSwitchedOff = true;
                }
            }
        });

        pressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                move(new randomMovement());
                startTimer();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        getLayoutSize();
    }

    public void getLayoutSize() {
        RelativeLayout rlayout = (RelativeLayout) findViewById(R.id.relative_layout);
        width = rlayout.getWidth();
        height = rlayout.getHeight();
    }

    public void setSwitchOn(){
        mySwitch.setOnCheckedChangeListener (null);
        mySwitch.setChecked(true);
        mySwitch.setOnCheckedChangeListener (mListener);
    }

    private void startTimer() {
        cTimer = new CountDownTimer(60000, 60000) { // 60000 after 1 min
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                setSwitchOn();
                cancelTimer();
            }
        };
        cTimer.start();
    }

    private void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

    public void move(MovementStrategy strategy){
        strategy.animateButton();
    }

    public interface MovementStrategy {
        void animateButton();
    }

    private class randomMovement implements MovementStrategy{

        @Override
        public void animateButton() {
            ViewPropertyAnimator viewPropertyAnimator = pressButton.animate();

            int randomX = new Random().nextInt(width-pressButton.getWidth());
            int randomY = new Random().nextInt(height-pressButton.getHeight());
            int switchWidh = mySwitch.getWidth();
            int switchHeight = mySwitch.getHeight();

            while(randomX > width - switchWidh && randomY > height - switchHeight){
                randomX = new Random().nextInt(width-pressButton.getWidth());
                randomY = new Random().nextInt(height-pressButton.getHeight());
            }

            if(isSwitchedOff){
                viewPropertyAnimator.setStartDelay(300);
            }
            else{
                viewPropertyAnimator.setStartDelay(0);
            }
            viewPropertyAnimator.translationX(randomX);
            viewPropertyAnimator.translationY(randomY);
        }
    }

    private class spiralMovement implements MovementStrategy{

        @Override
        public void animateButton() {
            //final Animator anim = ViewAnimationUtils.createCircularReveal(pressButton,) // pt createCircularReveal trebuie api 21
        }
    }
}
